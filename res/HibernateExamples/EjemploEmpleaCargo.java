import java.util.Iterator;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import primero.Departamentos;
import primero.Empleados;
import primero.HibernateUtil;

public class EjemploEmpleaCargo {

	public static void main(String[] args) {
		// 
		SessionFactory sesion = HibernateUtil.getSessionFactory();
		Session session = sesion.openSession();
			
		String hql = "from Empleados as emp right join emp.empleacargo order by emp.empNo";
		Query cons = session.createQuery(hql);
		Iterator q = cons.iterate();
		
		while (q.hasNext()) {
			Object[] par = (Object[]) q.next();
			Empleados dir = (Empleados) par[0];//director
			Empleados em = (Empleados) par[1]; //empleado
			if(dir!=null)				
			System.out.printf("Empleado: %d, %s, DIRECTOR: %d, %s %n",					
					em.getEmpNo(), em.getApellido(), 
					dir.getEmpNo(), dir.getApellido());
			else
				System.out.printf("Empleado %d, %s, SIN DIRECTOR.%n",					
						em.getEmpNo(), em.getApellido());
					
		}
		
		session.close();
		System.exit(0);
		
	}

}
