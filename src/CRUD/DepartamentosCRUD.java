package CRUD;

import model.DepartamentosEntity;
import org.hibernate.ObjectNotFoundException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.exception.ConstraintViolationException;

public class DepartamentosCRUD {
    private static Session session;
    // CRUDS
    public static void createDepartamentos(DepartamentosEntity departamento){
        Transaction tx = session.beginTransaction();
        try {
            session.save(departamento);
            tx.commit();
        } catch (ConstraintViolationException e) {
            System.out.println("DEPARTAMENTO DUPLICADO");
            System.out.printf("MENSAJE: %s%n",e.getMessage());
            System.out.printf("COD ERROR: %d%n",e.getErrorCode());
            System.out.printf("ERROR SQL: %s%n" ,
                    e.getSQLException().getMessage());
        }

    }
    public static DepartamentosEntity getDepartamentoById(int id){
        return session.load(DepartamentosEntity.class,id); //carga el departamento
    }
    public static void updateDepartamento(DepartamentosEntity departamento){
        Transaction tx = session.beginTransaction();
        DepartamentosEntity dep = session.load(DepartamentosEntity.class,departamento.getDeptNo());
        if(dep == null){
            System.out.println("El departamento no existe en la base de datos\nSaliendo...");
        }else{
            // reemplazar datos
            dep.setDnombre(departamento.getDnombre());
            dep.setLoc(departamento.getLoc());
            dep.setEmpleadosByDeptNo(departamento.getEmpleadosByDeptNo());
            // guardar
            session.save(dep);
            try {
                tx.commit();
            } catch (ConstraintViolationException e) {
                System.out.println("DEPARTAMENTO DUPLICADO");
                System.out.printf("MENSAJE: %s%n",e.getMessage());
                System.out.printf("COD ERROR: %d%n",e.getErrorCode());
                System.out.printf("ERROR SQL: %s%n" ,
                        e.getSQLException().getMessage());
            }
        }
    }
    public static void deleteDepartamentoById(int id){
        Transaction tx = session.beginTransaction();
        DepartamentosEntity dep = session.load(DepartamentosEntity.class,id);
        try{
            session.delete(dep); // elimina el objeto
            tx.commit();
            System.out.println("Departamento eliminado");
        } catch (ObjectNotFoundException o) {
            System.out.println("NO EXISTE EL DEPARTAMENTO...");
        } catch (ConstraintViolationException c) {
            System.out.println("NO SE PUEDE ELIMINAR, TIENE EMPLEADOS...");
        }catch (Exception e) {
            System.out.println("ERROR NO CONTROLADO....");
            e.printStackTrace();
        }
    }
    // other methods
    public static void setSession(Session session) {
        DepartamentosCRUD.session = session;
    }
}
