package CRUD;

import model.EmpleadosEntity;
import org.hibernate.ObjectNotFoundException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.exception.ConstraintViolationException;

import javax.persistence.Query;
import java.math.BigDecimal;
import java.sql.Date;

public class EmpleadosCRUD {
    private static Session session;
    //CRUD
    public static void createEmpleado(EmpleadosEntity empleado){
        Transaction tx = session.beginTransaction();
        try{
            session.save(empleado);
            tx.commit();
        }catch (ConstraintViolationException e){
            System.out.println("ERROR WHEN CREATE A NEW EMPLEADO\n"+e.getMessage());
        }
    }
    public static EmpleadosEntity getEmpleadoById(int id){
        return session.load(EmpleadosEntity.class,id);
    }
    public static EmpleadosEntity getEmpleadoByName(String name){
        Query query = session.createQuery("from EmpleadosEntity as e where e.apellido = :name");
        query.setParameter("name",name);
        EmpleadosEntity emp = (EmpleadosEntity) query.getSingleResult();
        if(emp == null){
            System.out.println("Empleado con nombre"+name+"no existe en la base de datos\nsaliendo...");
            return null;
        }
        return emp;
    }
    public static void updateEmpleado(EmpleadosEntity empleado){
        Transaction tx = session.beginTransaction();
        EmpleadosEntity emp = session.load(EmpleadosEntity.class,empleado.getEmpNo());
        if(emp == null){
            System.out.println("El empleado no existe en la base de datos\nsaliendo...");
        }else{
            // reemplazar datos
            emp.setEmpNo(empleado.getEmpNo());
            emp.setApellido(empleado.getApellido());
            emp.setSalario(empleado.getSalario());
            emp.setComision(empleado.getComision());
            emp.setDir(empleado.getDir());
            emp.setFechaAlt(empleado.getFechaAlt());
            emp.setOficio(empleado.getOficio());
            emp.setDepartamentosByDeptNo(emp.getDepartamentosByDeptNo());
            // guardar
            session.save(emp);
            try{
                tx.commit();
            }catch (ConstraintViolationException e){
                System.out.println("Empleado duplicado\n"+e.getMessage());
            }
        }
    }

    public static void deleteEmpleadoById(int id){
        Transaction tx= session.beginTransaction();
        EmpleadosEntity emp = new EmpleadosEntity();
        try{
            session.delete(emp);
            tx.commit();
            System.out.println("Empleado eliminado con exito!");
        } catch (ObjectNotFoundException o) {
            System.out.println("NO EXISTE EL EMPLEADO...");
        } catch (ConstraintViolationException c) {
            System.out.println("NO SE PUEDE ELIMINAR\n"+c.getMessage());
        }catch (Exception e) {
            System.out.println("ERROR NO CONTROLADO....");
            e.printStackTrace();
        }
    }
    // others
    public static void setSession(Session session) {
        EmpleadosCRUD.session = session;
    }
}
