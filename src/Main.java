import CRUD.DepartamentosCRUD;
import CRUD.EmpleadosCRUD;
import model.DepartamentosEntity;
import model.EmpleadosEntity;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import utils.Menu;

import javax.persistence.Query;
import java.math.BigDecimal;
import java.sql.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;

public class Main {
    private static final SessionFactory ourSessionFactory;

    static {
        try {
            Configuration configuration = new Configuration();
            configuration.configure();

            ourSessionFactory = configuration.buildSessionFactory();
        } catch (Throwable ex) {
            throw new ExceptionInInitializerError(ex);
        }
    }

    public static Session getSession() throws HibernateException {
        return ourSessionFactory.openSession();
    }

    public static void main(final String[] args) throws Exception {
        final Session session = getSession();

        final String[] options = {"Create 2 news Departments","Create an employee to each new department",
        "Update department 20","Update employee No 7499","Delete empleado called SALA",
        "Get Empleados who has 2000 of salary"};
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        DepartamentosCRUD.setSession(session);
        EmpleadosCRUD.setSession(session);
        try {
            Menu menu = new Menu(options,scanner){
                @Override
                public void goToMethod(int option) {
                    switch (option){
                        case 1:
                            ex1CreateTwoNewDeps();
                            break;
                        case 2:
                            ex2InsertEmployeeToNewDepartments();
                            break;
                        case 3:
                            ex3UpdateDepartment(20);
                            break;
                        case 4:
                            ex4UpdateEmpleado(7499);
                            break;
                        case 5:
                            ex5DeleteEmpleado("SALA");
                            break;
                        case 6:
                            ex6GetEmpleados2000Salary();
                            break;
                    }
                }
            };
            menu.start();
        } finally {
            session.close();
        }
    }

    /**
     * Exercici 1. Inserta aquests dos nous departaments a la taula departamentos
     */
    private static void ex1CreateTwoNewDeps() {
        DepartamentosEntity dep1 = new DepartamentosEntity();
        dep1.setDeptNo(60);
        dep1.setDnombre("TECNOLOGIA");
        dep1.setLoc("BARCELONA");

        DepartamentosEntity dep2 = new DepartamentosEntity();
        dep2.setDeptNo(70);
        dep2.setDnombre("INFORMATICA");
        dep2.setLoc("SEVILLA");

        DepartamentosCRUD.createDepartamentos(dep1);
        DepartamentosCRUD.createDepartamentos(dep2);
    }

    /**
     * Exercici 2. Inserta un nou empleat a cada departament nou, tria tu les dades dels nous empleats.
     */
    private static void ex2InsertEmployeeToNewDepartments() {
        // vars
        DepartamentosEntity dep1 = new DepartamentosEntity();
        dep1.setDeptNo(60);
        DepartamentosEntity dep2 = new DepartamentosEntity();
        dep2.setDeptNo(70);

        EmpleadosEntity emp1 = new EmpleadosEntity();
        emp1.setEmpNo(4);
        emp1.setApellido("Zaquinaula");
        emp1.setSalario(BigDecimal.valueOf(650));
        emp1.setComision(BigDecimal.valueOf(20));
        emp1.setDir(2);
        emp1.setFechaAlt(Date.valueOf("1998-05-06"));
        emp1.setOficio("Programmer");
        emp1.setDepartamentosByDeptNo(dep1);

        EmpleadosEntity emp2 = new EmpleadosEntity();
        emp2.setEmpNo(5);
        emp2.setApellido("Ramon");
        emp2.setSalario(BigDecimal.valueOf(1200));
        emp2.setComision(BigDecimal.valueOf(40));
        emp2.setDir(2);
        emp2.setFechaAlt(Date.valueOf("1997-05-06"));
        emp2.setOficio("Programmer");
        emp2.setDepartamentosByDeptNo(dep2);

        // save to ddbb
        EmpleadosCRUD.createEmpleado(emp1);
        EmpleadosCRUD.createEmpleado(emp2);
    }

    /**
     * Exercici 3. Actualitza el nom del departament número 20, ara es dirà RECERCA.
     */
    private static void ex3UpdateDepartment(int id){
        DepartamentosEntity dep  = DepartamentosCRUD.getDepartamentoById(20);
        DepartamentosEntity ancientDep = dep;
        if(dep != null){
            dep.setDnombre("RECERCA");
            DepartamentosCRUD.updateDepartamento(dep);
            System.out.println("Departamento con id "+dep.getDeptNo()+"actualizado con exito!");
            System.out.println("Viejo departamento \n"+ancientDep);
            System.out.println("Nuevo departamento \n"+dep);
        }
    }

    /**
     * Exercici 4. Actualitza el salari de l’empleat amb codi 7499, ara cobra 2100.
     */
    private static void ex4UpdateEmpleado(int id){
        EmpleadosEntity emp = EmpleadosCRUD.getEmpleadoById(id);
        if(emp != null) {
            EmpleadosEntity oldEmp = EmpleadosCRUD.getEmpleadoById(id);;
            emp.setSalario(BigDecimal.valueOf(2100));
            EmpleadosCRUD.updateEmpleado(emp);
            System.out.println("Empleado actualizado con exito");
            System.out.println("Datos antiguos\n"+oldEmp);
            System.out.println("Datos nuevos\n"+emp);
        }

    }

    /**
     * Excercici 5. Elimina l’empleat que es diu SALA.
     * @param name nom del empleat
     */
    private static void ex5DeleteEmpleado(String name) {
        EmpleadosEntity emp = EmpleadosCRUD.getEmpleadoByName(name);
        if(emp != null){
            EmpleadosCRUD.deleteEmpleadoById(emp.getEmpNo());
        }
    }

    /**
     * Excercici 6. Fes una consulta per mostrar els empleats que cobrin més de 2000.
     */
    private static void ex6GetEmpleados2000Salary(){
        Session session = getSession();
        Transaction tx = session.beginTransaction();
        Query query = session.createQuery("from EmpleadosEntity as e where e.salario >= 2000");
        List<EmpleadosEntity> empleados =  query.getResultList();
        for (EmpleadosEntity emp : empleados) {
            System.out.println("\n"+emp);
        }
    }

}